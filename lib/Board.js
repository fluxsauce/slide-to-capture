const _ = require('lodash');
const Coordinates = require('./Coordinates');

module.exports = class Board {
  constructor(squares) {
    this.squares = squares;
    this.captured = [];
    this.capturedThisTurn = [];
  }

  render(turn) {
    const retval = [];

    // Determine dimensions.
    let xMax;
    let yMax;
    let xMin;
    let yMin;

    this.squares.forEach((square) => {
      if (square.coordinates.x < xMin || xMin === undefined) {
        xMin = square.coordinates.x;
      }
      if (square.coordinates.x > xMax || xMax === undefined) {
        xMax = square.coordinates.x;
      }
      if (square.coordinates.y < yMin || yMin === undefined) {
        yMin = square.coordinates.y;
      }
      if (square.coordinates.y > yMax || yMax === undefined) {
        yMax = square.coordinates.y;
      }
    });

    const width = (xMax - xMin) + 1;
    const line = '-'.repeat(width * 6);

    // Summary.
    let summary = `Turn: ${turn}`;
    if (turn === 0) {
      summary += ' - Start';
    }
    if (this.capturedThisTurn.length > 0) {
      summary += ` - Captured ${this.capturedThisTurn.map(piece => piece.name).join(', ')}`;
    }
    if (this.goalReached()) {
      summary += ' - Goal';
    }

    retval.push(summary);
    retval.push(line);

    // Render the board itself.
    for (let y = yMin; y <= yMax; y += 1) {
      // Create the row which defaults to empty cells.
      const row = Array(width).fill(' '.repeat(6));

      for (let x = xMax; x >= xMin; x -= 1) {
        // Default to an empty character.
        let character = '';

        // Get square, if any.
        const square = this.getSquare(new Coordinates(x, y));
        if (square !== undefined) {
          if (square.contents && square.contents.render) {
            character = square.contents.render();
          } else if (square.goal) {
            character = '> ';
          } else if (square.start) {
            character = '< ';
          } else {
            character = '. ';
          }
        }

        // If a rendered character exists, replace it.
        if (character) {
          row[width - x - 1] = `  ${character}  `;
        }
      }
      // Render the row.
      retval.push(row.join(''));
    }

    // Final line.
    retval.push(line);

    // Combine and return.
    return retval.join('\n');
  }

  move(direction) {
    const start = this.findCoordinates('player');
    const end = start.calculate(direction);

    const result = [];

    let squareStart;
    let squareEnd;

    // Stupid brute force.
    this.squares.forEach((square) => {
      if (square.coordinates.x === start.x && square.coordinates.y === start.y) {
        squareStart = _.clone(square);
      } else if (square.coordinates.x === end.x && square.coordinates.y === end.y) {
        squareEnd = _.clone(square);
      } else {
        result.push(_.clone(square));
      }
    });

    // Capture.
    this.capturedThisTurn = [];
    if (squareEnd.contents !== null) {
      this.captured.push(squareEnd.contents);
      this.capturedThisTurn.push(squareEnd.contents);
    }
    // Set destination.
    squareEnd.contents = squareStart.contents;
    // Note: does not preserve start place.
    squareStart.contents = null;

    result.push(squareStart);
    result.push(squareEnd);

    this.squares = result;
  }

  getSquare(coordinates) {
    for (let i = 0; i < this.squares.length; i += 1) {
      const square = this.squares[i];
      if (_.isEqual(square.coordinates, coordinates)) {
        return square;
      }
    }
    return undefined;
  }

  findCoordinates(type) {
    for (let i = 0; i < this.squares.length; i += 1) {
      switch (type) {
        case 'player':
          if (this.squares[i].contents && this.squares[i].contents.constructor.name === 'Player') {
            return this.squares[i].coordinates;
          }
          break;

        case 'goal':
          if (this.squares[i].goal) {
            return this.squares[i].coordinates;
          }
          break;

        default:
          throw new TypeError(`unknown type: ${type}`);
      }
    }
    throw new Error(`Unable to find ${type}`);
  }

  goalReached() {
    const coordinatesGoal = this.findCoordinates('goal');
    const coordinatesPlayer = this.findCoordinates('player');

    return _.isEqual(coordinatesGoal, coordinatesPlayer);
  }
};
