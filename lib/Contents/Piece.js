module.exports = class Piece {
  constructor({ name, direction }) {
    this.name = name;
    this.direction = direction;
  }

  render() {
    const arrows = {
      east: '→',
      west: '←',
      north: '↑',
      south: '↓',
    };

    return `${this.name.substr(0, 1)}${arrows[this.direction]}`;
  }
};
