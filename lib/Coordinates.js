module.exports = class Coordinates {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  calculate(direction) {
    switch (direction) {
      case 'north':
        return new Coordinates(this.x, this.y - 1);
      case 'south':
        return new Coordinates(this.x, this.y + 1);
      case 'west':
        return new Coordinates(this.x + 1, this.y);
      case 'east':
        return new Coordinates(this.x - 1, this.y);
      default:
        throw new TypeError(`Unknown direction: ${direction}`);
    }
  }
};
