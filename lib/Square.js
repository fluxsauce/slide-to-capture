module.exports = class Square {
  constructor({
    coordinates, contents, start, goal,
  }) {
    this.coordinates = coordinates;
    this.contents = contents || null;
    this.start = start || false;
    this.goal = goal || false;
  }
};
