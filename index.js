/* eslint no-console: 0 */

const _ = require('lodash');
const Board = require('./lib/Board');
const level0101 = require('./levels/01_01');
const level0102 = require('./levels/01_02');
const level0103 = require('./levels/01_03');

const levels = [
  { label: '01_01', data: level0101 },
  { label: '01_02', data: level0102 },
  { label: '01_03', data: level0103 },
];

levels.forEach((level) => {
  console.log(`Level ${level.label}`);

  const boards = [];

  // Start.
  const board = new Board(level.data.squares);
  boards.push(_.clone(board));

  level.data.moves.forEach((move) => {
    board.move(move);
    boards.push(_.clone(board));
  });

  console.log();
  for (let turn = 0; turn < boards.length; turn += 1) {
    console.log(boards[turn].render(turn));
    console.log();
  }
});

// Notes
// squares shouldn't know their Coordinates
// Moves
// A move mutates the board
// Example:

// Order of Resolutions:
// - Player Moves
// - Each piece resolves its action

// Game states:
// - Player moves avatar ('avatar')
// - Player executes distraction ('distract')
