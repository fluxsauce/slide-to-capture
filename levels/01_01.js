const Coordinates = require('../lib/Coordinates');
const Square = require('../lib/Square');
const Player = require('../lib/Contents/Player');

module.exports = {
  squares: [
    new Square({ coordinates: new Coordinates(2, 0), contents: new Player(), start: true }),
    new Square({ coordinates: new Coordinates(1, 0) }),
    new Square({ coordinates: new Coordinates(0, 0), goal: true }),
  ],
  moves: [
    'east',
    'east',
  ],
};
