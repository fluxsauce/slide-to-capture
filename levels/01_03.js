const Coordinates = require('../lib/Coordinates');
const Square = require('../lib/Square');
const Player = require('../lib/Contents/Player');
const Piece = require('../lib/Contents/Piece');

module.exports = {
  squares: [
    new Square({ coordinates: new Coordinates(0, 1), contents: new Player(), start: true }),
    new Square({ coordinates: new Coordinates(1, 1) }),
    new Square({ coordinates: new Coordinates(1, 2) }),
    new Square({ coordinates: new Coordinates(2, 1) }),
    new Square({ coordinates: new Coordinates(2, 2) }),
    new Square({ coordinates: new Coordinates(3, 1), contents: new Piece({ name: 'Guard', direction: 'east' }) }),
    new Square({ coordinates: new Coordinates(3, 2) }),
    new Square({ coordinates: new Coordinates(2, 0), goal: true }),
  ],
  moves: [
    'west',
    'south',
    'west',
    'west',
    'north',
    'east',
    'north',
  ],
};
