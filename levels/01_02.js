const Coordinates = require('../lib/Coordinates');
const Square = require('../lib/Square');
const Player = require('../lib/Contents/Player');
const Piece = require('../lib/Contents/Piece');

module.exports = {
  squares: [
    new Square({ coordinates: new Coordinates(0, 2), contents: new Player(), start: true }),
    new Square({ coordinates: new Coordinates(1, 2) }),
    new Square({ coordinates: new Coordinates(2, 2), contents: new Piece({ name: 'Guard', direction: 'west' }) }),
    new Square({ coordinates: new Coordinates(3, 2) }),
    new Square({ coordinates: new Coordinates(3, 1) }),
    new Square({ coordinates: new Coordinates(3, 0), goal: true }),
  ],
  moves: [
    'west',
    'west',
    'west',
    'north',
    'north',
  ],
};
